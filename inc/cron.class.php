<?php

/**
 -------------------------------------------------------------------------
 Telegram plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/telegram
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Telegram.

 Telegram is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Telegram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Telegram. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginTelegramCron {

   static function getTypeName($nb = 0) {
      return PluginTelegramTr::__('Задания Telegram');
   }

   static function log($event, $level = 4) {
      Event::log(-1, 'plugin', $level, 'telegram', $event);
   }

   static function cronInfo($name) {
      switch ($name) {
         case 'SendNotification':
            return ['description' => PluginTelegramTr::__('Send message to Telegram')];
         case 'CheckWithoutOwner':
            return ['description' => PluginTelegramTr::__('Checking ticket without owner')];
      }
      return [];
   }

   static function cronSendNotification($task = null) {
      $notify = new PluginTelegramNotification();
      $count = 0;
      if ($notify->isWeekday()) {
         $count = $notify->sendAll();
      }
      $msg = sprintf(PluginTelegramTr::__('%s messages added to queue'), $count);
      if (is_object($task)) {
         $task->log($msg);
      } else {
         self::log($msg);
      }
      return 1;
   }

   static function cronCheckWithoutOwner($task = null) {

      $ticket = new Ticket();
      $ticket_user = new Ticket_User();
      $status = [Ticket::SOLVED, Ticket::CLOSED];
      $condition = "`is_deleted`='0' AND `status` NOT IN (". join(',', $status) . ") AND `itilcategories_id` NOT IN (" . join(',', PLUGIN_TELEGRAM_EXLUDE) . ")";

      $users_data = $ticket->find($condition);
      $msg = '';

      foreach ($users_data as $data) {
         $condition = "`tickets_id`='{$data['id']}' AND `type`='" . $ticket_user::ASSIGN . "'";
         $actors = $ticket_user->find($condition);
         if (empty($actors)) {
            $msg .= "[{$data['id']}] {$data['name']}\n";
         }
      }

      if (!empty($msg)) {
         $msg = PluginTelegramTr::__('Не назначенные на пользователей заявки:') . "\n$msg";
         $notify = new PluginTelegramNotification();
         $fields = [
            'itemtype'   => 'Ticket',
            'items_id'   => 0,
            'message'    => $msg,
         ];
         $notify->add($fields);
         $msg = PluginTelegramTr::__('Message added to queue');
         if (is_object($task)) {
            $task->log($msg);
         } else {
            self::log($msg);
         }
      }

      return 1;

   }

   static function intervalInSeconds(DateInterval $interval) {
      // возвращает бредовый результат
      // но большей точности и не нужно
      return $interval->y * 31536000 + $interval->m * 2592000 + $interval->d * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;
   }

}

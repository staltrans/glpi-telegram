<?php

/**
 -------------------------------------------------------------------------
 Telegram plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/telegram
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Telegram.

 Telegram is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Telegram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Telegram. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginTelegramHook {

   static function itemAddCommonITILObject($item) {

      $content = '';
      if (!empty($item->fields['content'])) {
         $content = Html::resume_name(Html::clean($item->fields['content']), 600);
      }

      $username = '';
      if (!empty($item->fields['users_id_recipient'])) {
         $user = new User();
         $user->getFromDB($item->fields['users_id_recipient']);
         $username = $user->getName();
      }

      if (!in_array($item->fields['itilcategories_id'], PLUGIN_TELEGRAM_EXLUDE)) {

         $msg = '';
         switch ($item->getType()) {
            case 'Ticket':
               $msg .= sprintf(
                  PluginTelegramTr::__("[%s] Новая заявка #%s от пользователя %s:\n%s"),
                  $item->fields['date_mod'],
                  $item->fields['id'],
                  $username,
                  $content
               );
               break;
            case 'Problem':
               $msg .= sprintf(
                  PluginTelegramTr::__("[%s] Новая проблема #%s от пользователя %s:\n%s"),
                  $item->fields['date_mod'],
                  $item->fields['id'],
                  $username,
                  $content
               );
               break;
            case 'Change':
               $msg .= sprintf(
                  PluginTelegramTr::__("[%s] Новое изменение #%s от пользователя %s:\n%s"),
                  $item->fields['date_mod'],
                  $item->fields['id'],
                  $username,
                  $content
               );
               break;
         }
         if (isset($msg)) {
            $notify = new PluginTelegramNotification();
            $fields = [
               'itemtype'   => $item->getType(),
               'items_id'   => $item->fields['id'],
               'message'    => $msg,
            ];
            $notify->add($fields);
         }

      }

      return true;

   }

   static function itemUpdateCommonITILObject($item) {

      $type = $item->getType();
      if (isset($item->oldvalues['status'])) {
         $status = $item::getStatus($item->fields['status']);
         switch ($type) {
            case 'Problem':
               $msg = sprintf(PluginTelegramTr::__("[%s]\nThe status of the problem #%s has changed to \"%s\""), $item->fields['date_mod'], $item->fields['id'], $status);
               break;
            case 'Change':
               $msg = sprintf(PluginTelegramTr::__("[%s]\nThe status of the change #%s has changed to \"%s\""), $item->fields['date_mod'], $item->fields['id'], $status);
               break;
         }
      }

      if (isset($msg)) {
         $notify = new PluginTelegramNotification();
         $fields = [
            'itemtype'   => $type,
            'items_id'   => $item->fields['id'],
            'message'    => $msg,
         ];
         $notify->add($fields);
      }

      return true;
   }

}

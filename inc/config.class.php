<?php

/**
 -------------------------------------------------------------------------
 Telegram plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/telegram
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Telegram.

 Telegram is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Telegram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Telegram. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginTelegramConfig extends PluginConfigVariable {

   const VAR_SETTINGS = 'common';
   const PLUGIN       = 'telegram';

   //static $rightname      = 'plugin_telegram_config';
   protected $displaylist = false;

   static function getTypeName($nb = 0) {
      return PluginTelegramTr::__('Настройки Telegram бота');
   }

   static function getMenuName() {
      return PluginTelegramTr::__('Telegram бот');
   }

   function showFormMain() {

      $token = '';
      if (isset($this->fields['value']->token)) {
         $token = $this->fields['value']->token;
      }
      $ticket_chid = '';
      if (isset($this->fields['value']->ticket_chid)) {
         $ticket_chid = $this->fields['value']->ticket_chid;
      }
      $ch_chid = '';
      if (isset($this->fields['value']->ch_chid)) {
         $ch_chid = $this->fields['value']->ch_chid;
      }
      $pr_chid = '';
      if (isset($this->fields['value']->pr_chid)) {
         $pr_chid = $this->fields['value']->pr_chid;
      }
      $calendars_id = '';
      if (isset($this->fields['value']->calendars_id)) {
         $calendars_id = $this->fields['value']->calendars_id;
      }

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginTelegramTr::__('Token') . '</td>';
      echo '<td>';
      echo '<input type="hidden" name="name" value="' . self::VAR_SETTINGS . '">';
      echo '<input type="hidden" name="plugin" value="' .  self::PLUGIN . '">';
      echo '<input type="text" name="value[token]" size="60" value="' . $token  . '">';
      echo '</td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginTelegramTr::__('Chat ID for tickets') . '</td>';
      echo '<td><input type="text" name="value[ticket_chid]" size="60" value="' . $ticket_chid . '"></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginTelegramTr::__('Chat ID for changes') . '</td>';
      echo '<td><input type="text" name="value[ch_chid]" size="60" value="' . $ch_chid . '"></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . PluginTelegramTr::__('Chat ID for problems') . '</td>';
      echo '<td><input type="text" name="value[pr_chid]" size="60" value="' . $pr_chid . '"></td>';
      echo '</tr>';

      echo '<tr class="tab_bg_2">';
      echo '<td colspan="2">' . __('Calendar') . '</td>';
      echo '<td>';
      Calendar::dropdown([
         'name' => 'value[calendars_id]',
         'value' => $calendars_id,
      ]);
      echo '</td>';
      echo '</tr>';

   }

}

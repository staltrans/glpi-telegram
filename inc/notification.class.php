<?php

/**
 -------------------------------------------------------------------------
 Telegram plugin for GLPI
 Copyright (C) 2018 by the Staltrans Development Team.

 https://bitbucket.org/staltrans/telegram
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Telegram.

 Telegram is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Telegram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Telegram. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginTelegramNotification extends CommonDBTM {

   const TICKET  = 'Ticket';
   const CHANGE  = 'Change';
   const PROBLEM = 'Problem';

   private $token = '';
   private $ticket_chid = '';
   private $ch_chid = '';
   private $pr_chid = '';
   private $calendars_id = '';
   private $bot;

   function __construct() {
      $config = new PluginTelegramConfig();
      if ($vals = $config->get($config::VAR_SETTINGS)) {
         $this->token = $vals->token;
         $this->ticket_chid = $vals->ticket_chid;
         $this->ch_chid = $vals->ch_chid;
         $this->pr_chid = $vals->pr_chid;
         $this->calendars_id = $vals->calendars_id;
      }
      $this->bot = new \TelegramBot\Api\BotApi($this->token);
   }

   private function getChIDByChType($ch_type) {
      switch ($ch_type) {
         case self::TICKET:
            return $this->ticket_chid;
         case self::CHANGE:
            return $this->ch_chid;
         case self::PROBLEM:
            return $this->pr_chid;
      }
   }

   function isWeekday() {
      $calendar = new Calendar();
      if ($calendar->getFromDB($this->calendars_id)) {
         return !$calendar->isHoliday(date("Y-m-d H:i:s"));
      }
      return true;
   }

   function sendMessage($ch_type, $text) {
      try {
         $this->bot->sendMessage($this->getChIDByChType($ch_type), $text);
         return true;
      } catch (\TelegramBot\Api\Exception $e) {
         return false;
      }
   }

   function sendAll() {
      $items = $this->find();
      $count = 0;
      foreach ($items as $item) {
         if ($this->sendMessage($item['itemtype'], $item['message'])) {
            if ($this->delete($item, 1)) {
               $count++;
            }
         }
      }
      return $count;
   }

}

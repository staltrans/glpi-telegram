<?php
/*
 -------------------------------------------------------------------------
 Telegram plugin for GLPI
 Copyright (C) 2017 by the Telegram Development Team.

 https://bitbucket.org/staltrans/telegram
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Telegram.

 Telegram is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Telegram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Telegram. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_telegram_install() {

   global $DB;

   $table_notifications = 'glpi_plugin_telegram_notifications';

   if (!$DB->tableExists($table_notifications)) {
      $query = "CREATE TABLE `$table_notifications` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT,
                  `itemtype` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
                  `items_id` int(11) DEFAULT NULL,
                  `message` LONGTEXT,
                  `date_mod` timestamp NULL DEFAULT NULL,
                  `date_creation` timestamp NULL DEFAULT NULL,
                  PRIMARY KEY(`id`),
                  KEY `itemtype` (`itemtype`),
                  KEY `items_id` (`items_id`),
                  KEY `date_mod` (`date_mod`),
                  KEY `date_creation` (`date_creation`)
               ) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_unicode_ci";

      $DB->queryOrDie($query);
   }

   $taskopt = [
      'allowmode' => CronTask::MODE_EXTERNAL,
      'mode'      => CronTask::MODE_EXTERNAL,
   ];

   CronTask::Register('PluginTelegramCron', 'SendNotification', MINUTE_TIMESTAMP, $taskopt);
   CronTask::Register('PluginTelegramCron', 'CheckWithoutOwner', 5 * MINUTE_TIMESTAMP, $taskopt);

   return true;

}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_telegram_uninstall() {

   global $DB;

   $tables = [
      'glpi_plugin_telegram_notifications'
   ];

   ProfileRight::deleteProfileRights([PluginTelegramConfig::$rightname]);

   foreach ($tables as $t) {
      if ($DB->tableExists($t)) {
         $query = "DROP TABLE `$t`";
         $DB->queryOrDie($query);
      }
   }

   CronTask::Unregister('SendNotification');
   CronTask::Unregister('CheckWithoutOwner');

   $cfg = new PluginTelegramConfig();
   $cfg->delAll();

   return true;

}

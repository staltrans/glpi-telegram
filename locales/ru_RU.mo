��          �      �           	     $     8     M     a     q  
   v     �     �     �     �     �     �     �          $     *     /     G     `  5   x  6   �  �  �  <   �  %   �  !   �       #   <     `     s  ,   �     �  /   �  +     1   4     f  $   v  6   �  
   �     �  ,   �  *     &   F  C   m  A   �                                       
                                    	                             %s messages added to queue Chat ID for changes Chat ID for problems Chat ID for tickets Common settings Edit Install %s Installed / not configured Module options Please activate %s Please install %s Send message to Telegram Telegram bot Telegram bot settings This plugin requires %s Token View [%s]
New change #%s:
%s [%s]
New problem #%s:
%s [%s]
New ticket #%s:
%s [%s]
The status of the change #%s has changed to "%s" [%s]
The status of the problem #%s has changed to "%s" Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-11-29 11:07+0500
PO-Revision-Date: 2017-11-29 11:08+0500
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
Last-Translator: boris_t <boris@talovikov.ru>
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
Language: ru
 %s сообщений добавленно в очередь ID чата для изменений ID чата для проблем ID чата для заявок Основные настройки Изменение Установить %s Установлен / не настроен Параметры модуля Пожалуйста, активируйте %s Пожалуста, установите %s Отправить сообщение в Telegram Telegram бот Настройки Telegram бота Для этого плагина требуется %s Токен Просмотр [%s]
Новое изменение №%s:
%s [%s]
Новая проблема №%s:
%s [%s]
Новая заявка №%s:
%s [%s]
Статус изменения №%s изменён на "%s" [%s]
Статус проблемы №%s изменён на "%s" 
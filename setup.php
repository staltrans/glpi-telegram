<?php
/*
 -------------------------------------------------------------------------
 Telegram plugin for GLPI
 Copyright (C) 2017 by the Telegram Development Team.

 https://bitbucket.org/staltrans/telegram
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Telegram.

 Telegram is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Telegram is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Telegram. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_TELEGRAM_VERSION', '0.11');
define('PLUGIN_TELEGRAM_EXLUDE', [69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 87, 88, 89, 90, 91, 92]);

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_telegram() {

   global $PLUGIN_HOOKS;

   //Plugin::registerClass('PluginTelegramProfile', ['addtabon' => 'Profile']);

   $PLUGIN_HOOKS['csrf_compliant']['telegram'] = true;

   $PLUGIN_HOOKS['menu_toadd']['telegram'] = ['plugins' => 'PluginTelegramConfig'];

   $PLUGIN_HOOKS['item_add']['telegram'] = [
      'Ticket'         => ['PluginTelegramHook', 'itemAddCommonITILObject'],
      'Problem'        => ['PluginTelegramHook', 'itemAddCommonITILObject'],
      'Change'         => ['PluginTelegramHook', 'itemAddCommonITILObject'],
   ];

   $PLUGIN_HOOKS['item_update']['telegram'] = [
      'Problem'        => ['PluginTelegramHook', 'itemUpdateCommonITILObject'],
      'Change'         => ['PluginTelegramHook', 'itemUpdateCommonITILObject'],
   ];

}

/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_telegram() {
   return [
      'name'           => 'Telegram',
      'version'        => PLUGIN_TELEGRAM_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-telegram',
      'minGlpiVersion' => '9.3'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_telegram_check_prerequisites() {
   $plugin = new Plugin();
   if (!$plugin->isInstalled('config')) {
      printf(__('This plugin requires %s', 'telegram'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('config')) {
      printf(__('Please activate %s', 'telegram'), '<a href="https://bitbucket.org/staltrans/glpi-config">Config GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isInstalled('libraries')) {
      printf(__('This plugin requires %s', 'telegram'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!$plugin->isActivated('libraries')) {
      printf(__('Please activate %s', 'telegram'), '<a href="https://bitbucket.org/staltrans/glpi-libraries">Libraries GLPI plugin</a>');
      return false;
   }
   if (!class_exists('\TelegramBot\Api\BotApi')) {
      printf(__('Please install %s', 'telegram'), '<a href="' . PluginLibrariesList::getURL() . '">telegram-bot/api</a>');
      return false;
   }
   // Strict version check (could be less strict, or could allow various version)
   if (version_compare(GLPI_VERSION, '9.3', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.3');
      } else {
         echo "This plugin requires GLPI >= 9.3";
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_telegram_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'telegram');
   }
   return false;
}

function plugin_telegram_libraries_composer_help() {
   return [
      'title' => sprintf(__('Install %s', 'telegram'), 'telegram-bot/api'),
      'content' => '[... how to install telegram-bot/api ...]'
   ];
}
